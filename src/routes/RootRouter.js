import { DrawerNavigator } from "react-navigation";
import SettingsPageContainer from "../page/SettingsPage";
import HomePageContainer from "../page/HomePage";
import LoginPageContainer from "../page/LoginPage";
import LiveStreamPageContainer from "../page/LiveStreamPage";
import LabPage from "../page/LabPage";
import SideMenu from "../components/sideMenu/SideMenu";

export default (RootRouter = DrawerNavigator(
  {
    Home: {
      screen: HomePageContainer,
      navigationOptions: () => ({
        drawerLockMode: "locked-open"
      })
    },
    Login: {
      screen: LoginPageContainer,
      navigationOptions: () => ({
        drawerLockMode: "locked-open"
      })
    },
    LiveStream: {
      screen: LiveStreamPageContainer
    },
    Lab: {
      screen: LabPage
    },
    Settings: {
      screen: SettingsPageContainer
    }
  },
  {
    drawerWidth: 300,
    contentComponent: SideMenu
  }
));
