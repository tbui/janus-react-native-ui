import React from 'react';
import { StyleSheet, View, Alert } from 'react-native'
import QRCodeScanner from 'react-native-qrcode-scanner';

export default class ScanPage extends React.Component {
    constructor() {
        super();
        this.state = {

        }
    }
    async componentWillMount() {

    }

    handleBarCodeRead = (data) => {
        const { navigation } = this.props;
        navigation.goBack();
        navigation.state.params.onSetResultScan(data.data);
    }

    render() {
        return (
            <View style={{ flex: 1, backgroundColor:"#000000" }}>
                <QRCodeScanner containerStyle={{ height: '100%', width: '100%' }}
                    onRead={this.handleBarCodeRead.bind(this)} showMarker={true} checkAndroid6Permissions={true} permissionDialogMessage="Ticket validation need camera permission"/>
            </View>
        );
    }
}