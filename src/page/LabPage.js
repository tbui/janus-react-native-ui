import React from "react";
import { View, Image } from "react-native";
import StyleSheets from "../utils/StyleSheets";
import Common from "../utils/Common";
import Header from "../components/Header";

export default class LabPage extends React.Component {
  constructor(props) {
    super(props);

    this.state = {};
  }

  goBack = () => {
    this.props.navigation.navigate("LiveStream");
  };

  render() {
    return (
      <View style={StyleSheets.container}>
        <Header
          onSettingsPress={() => {
            this.props.navigation.navigate("Settings");
          }}
          actions={[
            {
              key: 1,
              action: this.goBack,
              icon: require("../assets/images/ic_back.png")
            }
          ]}
          alignLogo="baseline"
          paddingLeftLogo={5}
          staffId={Common.STAFF_ID}
        />
        <View style={{ flex: 1, width: "100%", backgroundColor: "#ffffff" }}>
          <Image
            style={{ resizeMode: "stretch", width: "100%", height: "100%" }}
            source={require("../assets/images/bg_lab.png")}
          />
        </View>
      </View>
    );
  }
}
