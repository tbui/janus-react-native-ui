import React from "react";
import { StyleSheet, View, Alert, Dimensions, Text } from "react-native";

import Common from "../utils/Common";
import url from "../config/url";
import { setOrientation } from "../actions/liveStreamAction";
import CodeHelper from "../utils/CodeHelper";
import StyleSheets from "../utils/StyleSheets";
import Header from "../components/Header";
import GestureView from "../components/GestureView";
import MeasureView from "../components/MeasureView";
import RNImmediatePhoneCall from "react-native-immediate-phone-call";
import { connect } from "react-redux";
import Spinner from 'react-native-loading-spinner-overlay';
import {
  RTCPeerConnection,
  RTCMediaStream,
  RTCIceCandidate,
  RTCSessionDescription,
  RTCView,
  RTCVideoView,
  MediaStreamTrack,
  getUserMedia
} from "react-native-webrtc";
import Janus from "../lib/janus.mobile";

let server = url.wss;
let janus;
let streaming = null;
let started = false;

const VIDEO = 1;
const WAVEFORMS = 2;

Janus.init({
  debug: "all",
  callback: () => {
    if (started) return;
    started = true;
    janus = new Janus({
      iceServers: [url.stun, url.turn],
      server: server,
      error: error => {
        Janus.error(error);
        Alert.alert("WebRTC error... " + JSON.stringify(error));
      }
    });
  }
});

class LiveStreamPage extends React.Component {
  static PAGE_NAME = "LiveStreamPage";
  constructor() {
    super();

    this.state = {
      info: "Initializing",
      status: "init",
      videoStream: null,
      waveformStream: null,
      videoLoading: false,
      waveformLoading: false
    };
  }
  async componentDidMount() {
    this.restartVideos();
  }

  componentWillReceiveProps = newProps => {
    if (!CodeHelper.isNullOrEmpty(newProps.messageErrors)) {
      CodeHelper.showMessage("Errors", newProps.messageErrors);
      return;
    }
    if (!CodeHelper.isNull(newProps.responseValidate)) {
    }
  };

  startSession = callback => {
    janus.createSession(function() {
      if (callback) callback();
    });
  };

  startStream = index => {
    this.setVideoWaiting(index, true);

    var that = this;
    return new Promise((resolve, reject) => {
      if (!resolve) resolve = () => {};
      if (Object.keys(janus.getSessions()).length > 0) {
        that.attachPlugin(index);
        resolve();
      } else {
        janus.createSession(function() {
          that.attachPlugin(index);
          resolve();
        });
      }
    });
  };

  attachPlugin = index => {
    if(index === VIDEO)
      this.setState({videoLoading: true});
    else
      this.setState({waveformLoading: true});
    janus.attach({
      plugin: "janus.plugin.streaming",
      success: function(handle) {
        Janus.log(
          "Plugin attached! (" +
            handle.getPlugin() +
            ", id=" +
            handle.getId() +
            ")"
        );
        // start the stream
        var body = {
          request: "watch",
          id: index
        };
        handle.send({
          message: body
        });
      },
      error: error => {
        Janus.error("  -- Error attaching plugin... ", error);
        this.setVideoWaiting(index, false);
        alert("Error attaching plugin... " + error);
      },
      onmessage: (msg, jsep, handle) => {
        Janus.debug(" ::: Got a message :::");
        Janus.debug(msg);
        var result = msg["result"];
        if (result !== null && result !== undefined) {
          if (result["status"] !== undefined && result["status"] !== null) {
            var status = result["status"];
            if (status === "stopped") this.stopSession();
          } else if (msg["streaming"] === "event") {
            // Is simulcast in place?
          }
        } else if (msg["error"] !== undefined && msg["error"] !== null) {
          Alert.alert(msg["error"]);
          this.stopSession();
          return;
        }
        if (jsep !== undefined && jsep !== null) {
          Janus.debug("Handling SDP as well...");
          Janus.debug(jsep);
          // Offer from the plugin, let's answer
          handle.createAnswer({
            jsep: jsep,
            media: {
              audioSend: false,
              videoSend: false
            }, // We want recvonly audio/video
            success: function(jsep) {
              Janus.debug("Got SDP!");
              Janus.debug(jsep);
              var body = {
                request: "start"
              };
              handle.send({
                message: body,
                jsep: jsep
              });
              //
            },
            error: function(error) {
              Janus.error("WebRTC error:", error);
              Alert.alert("WebRTC error... " + JSON.stringify(error));
              this.setVideoWaiting(index, false);
            }
          });
        }
      },
      onremotestream: (stream, handle) => {
        Janus.debug(" ::: Got a remote stream :::");
        Janus.debug(stream);
        this.setVideoWaiting(index, false);
        if (index === VIDEO) {
          this.setState({
            status: "streaming-video",
            videoStream: stream.toURL(),
            videoLoading: false
          });
        } else if (index === WAVEFORMS) {
          this.setState({
            status: "streaming-wareforms",
            waveformStream: stream.toURL(),
            waveformLoading: false
          });
        }
      },
      oncleanup: () => {
        Janus.log(" ::: Got a cleanup notification :::");
        this.setState({
          videoStream: null,
          waveformStream: null
        });
      }
    });
  };

  stopSession = () => {
    this.setVideoWaiting(index, false);

    if (!janus) return;
    var handles = janus.getHandles();
    // use any handle to send the camera control request
    var handle = Object.keys(handles)[0];
    var body = {
      request: "stop"
    };
    handles[handle].send({
      message: body
    });
    handles[handle].hangup();
    janus.destroy();
    this.setState({
      status: "ready"
    });
  };

  setVideoWaiting = (index, isWaiting) => {
    if(index==VIDEO){
      if(this.state.videoWaiting != isWaiting)
        this.setState({videoWaiting: isWaiting});
    }else if(index==WAVEFORMS){
      if(this.state.waveformWaiting != isWaiting)
        this.setState({waveformWaiting: isWaiting});
    }
  };

  cameraControl = (action, duration) => {
    if (!janus || !janus.getHandles()) return;
    var handles = janus.getHandles();
    // use any handle to send the camera control request
    var handle = Object.keys(handles)[0];
    var body = {
      request: "camera_control",
      action: action,
      duration: duration
    };
    handles[handle].send({
      message: body
    });
  };
  moveDown = duration => {
    console.log(duration);
    this.cameraControl("movedown", duration);
  };
  moveUp = duration => {
    console.log(duration);
    this.cameraControl("moveup", duration);
  };
  moveLeft = duration => {
    console.log(duration);
    this.cameraControl("moveleft", duration);    
  };
  moveRight = duration => {
    console.log(duration);
    this.cameraControl("moveright", duration);    
  };
  zoomIn = duration => {
    console.log(duration);
    this.cameraControl("zoomin"), duration;    
  };
  zoomOut = duration => {
    console.log(duration);
    this.cameraControl("zoomout", duration);    
  };
  zoomStop = () => {
    this.cameraControl("zoomstop");
  };
  stopMove = () => {
    this.cameraControl("stopmove");
  };
  stopZoom = () => {
    this.cameraControl("stopzoom");
  };
  resetCam = () => {
    this.cameraControl("reset");
  };

  openLabView = () => {
    this.props.navigation.navigate("Lab");
  };
  callPhone = () => {
    this.startStream(WAVEFORMS);
    // RNImmediatePhoneCall.immediatePhoneCall("16172999852");
  };
  restartVideos = () => {
    this.startStream(VIDEO).then(() => {
      this.startStream(WAVEFORMS);
    });
  };
  _onLayout = event => {
    const { width, height } = event.nativeEvent.layout;
    const state = width > height ? "LANDSCAPE" : "PORTRAIT";
    this.props.setOrientation(state);
  };

  render() {
    return (
      <View style={[StyleSheets.container]} onLayout={this._onLayout}>
        {(this.props.orientation == "PORTRAIT") && (<Header
          onSettingsPress={() => {
            this.props.navigation.navigate("Settings");
          }}
          actions={[
            {
              key: 1,
              action: this.openLabView,
              icon: require("../assets/images/ic_lab.png")
            },
            {
              key: 2,
              action: this.callPhone,
              icon: require("../assets/images/ic_phone.png")
            },
            {
              key: 3,
              action: this.restartVideos,
              icon: require("../assets/images/ic_refresh.png")
            }
          ]}
          alignLogo="baseline"
          paddingLeftLogo={5}
          staffId={Common.STAFF_ID}
        />)}
        <View
          style={[
            StyleSheets.container,
            StyleSheets.dashBlueColor,
            { justifyContent: "flex-start", padding: 1 }
          ]}
        >
          <View
            style={[styles.cameraContainer]}
          >
            <Spinner visible={this.state.videoLoading} textStyle={{color: '#FFF'}} />              

            {(
              <GestureView
                style={{ flex: 1, width: "100%" }}
                onSwipeLeft={this.moveLeft}
                onSwipeRight={this.moveRight}
                onSwipeTop={this.moveUp}
                onSwipeBottom={this.moveDown}
                onZoomIn={this.zoomIn}
                onZoomOut={this.zoomOut}
                contentView={
                  <RTCView
                    key={Math.floor(Math.random() * 1000)}
                    streamURL={this.state.videoStream}
                    style={{ flex: 1 }}
                  />
                }
                waiting={this.state.videoWaiting}
              />
            )}
          </View>
          {(this.props.orientation == "PORTRAIT") && (
            <View
              style={{
                flex: 1,
                flexDirection: "row",
                backgroundColor: "#000000"
              }}
            >
              <View style={[{ flex: 4 }]}>
                {(
                  <GestureView
                    style={{ flex: 1 }}
                    contentView={
                      <RTCView
                        key={Math.floor(Math.random() * 1000)}
                        streamURL={this.state.waveformStream}
                        style={{ flex: 1 }}
                        waiting={this.state.waveformWaiting}
                      />
                    }
                  />
                )}
              </View>
              <View style={{ flex: 2 }}>
                <MeasureView
                  data={{ title: "ECG", value: "75", unit: "Beats/min" }}
                />
                <MeasureView
                  data={{ title: "SpO2", value: "100", unit: "%" }}
                />
                <MeasureView
                  data={{ title: "BP", value: "134/52", unit: "mmHg" }}
                />
                <MeasureView
                  data={{ title: "Respiratory", value: "9", unit: "per min" }}
                />
                <MeasureView
                  data={{
                    title: "Temperature",
                    value: "37.2",
                    unit: "Celcius"
                  }}
                />
                <MeasureView
                  data={{ title: "Vent", value: "22", unit: "22" }}
                />
                <MeasureView
                  data={{
                    title: "22",
                    value: "600 / 9 / 5",
                    unit: "TV  RR  Peep"
                  }}
                />
              </View>
            </View>
          )}
        </View>
        {this.props.waiting && (
          <View style={[StyleSheets.loading]}>
            <ActivityIndicator size="large" color="#0000ff" />
          </View>
        )}
      </View>
    );
  }
}

const mapStateToProps = state => ({
  waiting: state.liveStreamReducer.waiting,
  orientation: state.liveStreamReducer.orientation,
  responseLiveStream: state.liveStreamReducer.responseLiveStream,
  messageErrors: state.liveStreamReducer.messageErrors
});

const mapDispatchToProps = dispatch => {
  return {
    setOrientation: orientation => {
      dispatch(setOrientation(orientation));
    },
    liveStreamRequest: code => {
      dispatch(liveStreamRequest(code));
    }
  };
};
export default (LiveStreamPageContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(LiveStreamPage));

const styles = StyleSheet.create({
  cameraContainer: {
    flex: 1,
    flexDirection: "row",    
    height: 300,
    marginBottom: 1,
    backgroundColor: "#000000",
    alignSelf: "stretch"
  },
  remoteView: {
    width: Dimensions.get("window").width,
    height: Dimensions.get("window").height / 2.35
  }
});
