import React from 'react';
import { Picker, StyleSheet, View, Image, Text, TouchableOpacity, Dimensions, Alert } from 'react-native';
import CodeHelper from '../utils/CodeHelper';
import Common from '../utils/Common';
import ImageResources from '../utils/ImageResource';
import { connect } from 'react-redux';
import { refreshHomePage } from '../actions/homeAction'
import VersionNumber from 'react-native-version-number';

const { width, height } = Dimensions.get('window');
const MAIN_HEIGHT = height - 80 - 30 - 60;
class SettingsPage extends React.Component {
    
    constructor(props) {
        super(props)
        this.state = {
            language: Common.CURRENT_LANG,
            version: ''
        }
    }

    onLogOut = () => {
        this.props.navigation.navigate("Login");
    }
    onItemSelectedChanged = (itemValue, itemIndex) => {
        Alert.alert('Question', 'App need to restart to apply change language. Do you want to change it? ', [
            {
                text: 'Ok', onPress: async () => {
                    this.setState({ language: itemValue });
                    CodeHelper.setCurrentLang(itemValue).done();
                    this.props.navigation.popToTop();
                    this.props.refreshHomePage();
                }
            },
            { text: 'Cancel', onPress: () => { } }
        ], { cancelable: false });

    }
    render() {
        return (

            <View style={{ flex: 1, flexDirection: 'column' }} behavior="padding" enabled>
                <View style={{ height: 60, flexDirection: 'row', paddingTop: 20, marginLeft: 10, marginRight: 10 }}>
                    <Image
                        alignSelf='center'
                        source={require('../assets/images/ic_menu_black.png')}
                        style={{ resizeMode: 'contain', height: 25, width: 25, justifyContent: 'center', }}
                    />
                    <View style={{ flex: 1, flexDirection: 'column', justifyContent: 'center', alignItems: 'center' }}>
                        <Text style={{ fontSize: 20, fontWeight: 'bold' }}> Settings </Text>
                    </View>
                </View>
                <Text style={{ height: 2, backgroundColor: '#858687', marginLeft: 10, marginRight: 10 }} />
                <View style={{ flexDirection: 'column', marginLeft: 10, marginRight: 10, marginTop: 30, height: MAIN_HEIGHT }}>
                    <View style={{ flexDirection: 'row' }}>
                        <View style={{ flex: 1, flexDirection: 'column' }}>
                            <Text style={{ fontSize: 18 }}> Language </Text>
                        </View>
                        <Picker style={{ height: 50, width: 120, borderColor: '#000000', borderWidth: 5 }}
                            onValueChange={(itemValue, itemIndex) => this.onItemSelectedChanged(itemValue, itemIndex)}
                            selectedValue={this.state.language}>
                            <Picker.Item label="English" value="en" />
                            <Picker.Item label="Viet Nam" value="vn" />                            
                        </Picker>
                    </View>
                    <View style={{ flexDirection: 'row' }}>
                        <View style={{ flex: 1, flexDirection: 'column', }}>
                            <Text style={{ fontSize: 18 }}> Version </Text>
                        </View>

                        <Text>{VersionNumber.appVersion}</Text>
                    </View>
                </View>
                <TouchableOpacity onPress={this.onLogOut}>
                    <Image source={ImageResources.Images[Common.CURRENT_LANG].ic_logout} style={{ resizeMode: 'center', height: 50, width: '100%' }} />
                </TouchableOpacity>
            </View >

        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center',
    },
});


const mapStateToProps = state => ({
    refreshing: state.homeReducer.refreshing
});

const mapDispatchToProps = (dispatch) => {
    return {
        refreshHomePage: () => { dispatch(refreshHomePage()); }
    }
}
export default SettingsPageContainer = connect(mapStateToProps, mapDispatchToProps)(SettingsPage);