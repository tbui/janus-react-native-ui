import React from 'react';
import { View, Image, ActivityIndicator, Alert, PermissionsAndroid } from 'react-native';
import StyleSheets from '../utils/StyleSheets'
import CodeHelper from '../utils/CodeHelper';
import { connect } from 'react-redux';

class HomePage extends React.Component {
  static PAGE_NAME = "HomePage";
  _interval = null;
  componentWillMount() {
    CodeHelper.getCurrentLang().done();
  }
  
  render() {
    this.requestPermission();
    return (
      <View style={[StyleSheets.container, { backgroundColor: '#000000' }]}>
        <Image source={require('../assets/images/logo_dark.png')} style={{ resizeMode: 'center', width: '100%' }} />
        {this.props.waitting &&
          <View style={[StyleSheets.loading]}>
            <ActivityIndicator size="large" color="#0000ff" />
          </View>
        }
      </View>
    );
  }

  requestPermission = async () => {

    try {
      var permissions = [
        PermissionsAndroid.PERMISSIONS.CAMERA,
        PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
        PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE,
        PermissionsAndroid.PERMISSIONS.CALL_PHONE];
      var results = await PermissionsAndroid.requestMultiple(permissions);
      var isGrantedAll = true;
      permissions.map(per => {
        var r = results[per];
        console.log("RESULT", r)
        if (r == PermissionsAndroid.RESULTS.DENIED)
          isGrantedAll = false;
        return;
      })
      if (isGrantedAll) {
        this.startTimer();
      } else {
        Alert.alert("Warnning", "Please accept permissions to continue!!!",
          [{
            text: "Ok", onPress: () => {
              this.setState({ reRender: !this.state.reRender })
            }
          }], { cancelable: false })
      }
    } catch (err) {
      console.warn(err)
      this.startTimer();
    }
  }

  startTimer = () => {
    console.log("startTimer");
    var intervalTime = 1000;    
    this._interval = setInterval(() => {
      this.props.navigation.navigate("Login");
      clearInterval(this._interval);
    }, intervalTime);
  }
}

const mapStateToProps = state => ({
  refreshing: state.homeReducer.refreshing,
  waitting: state.homeReducer.waitting
});

const mapDispatchToProps = (dispatch) => {
  return { }
}
export default HomePageContainer = connect(mapStateToProps, mapDispatchToProps)(HomePage);
