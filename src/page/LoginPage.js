import React from "react";
import {
  TouchableOpacity,
  Text,
  View,
  Image,
  TextInput,
  Animated,
  Keyboard,
  KeyboardAvoidingView,
  ActivityIndicator,
  Alert,
  Dimensions
} from "react-native";
import Common from "../utils/Common";
import CodeHelper from "../utils/CodeHelper";
import StyleSheets from "../utils/StyleSheets";
import ImageResources from "../utils/ImageResource";
import LangResources from "../utils/LangResources";
import { connect } from "react-redux";
import { setStaffId, setPassword, loginRequest } from "../actions/loginAction";

class LoginPage extends React.Component {
  static PAGE_NAME = "LoginPage";
  constructor(props) {
    super(props);
  }

  onLogin = () => {
    this.login();
  };
  onStaffIdChange = text => {
    this.props.setStaffId(text);
  };
  onPasswordChange = text => {
    this.props.setPassword(text);
  };

  componentWillMount() {}

  componentWillReceiveProps(nextProps) {
    var response = nextProps.response;
    // if (!CodeHelper.isNull(response)) {
      this.loginComplete(response);
    // }
    var messageErrors = nextProps.messageErrors;
    if (!CodeHelper.isNullOrEmpty(messageErrors)) {
      CodeHelper.showMessage(messageErrors);
    }
  }

  componentWillUnmount() {}

  openMainView = () => {
    this.props.navigation.navigate("LiveStream");
  };

  validLogin = () => {
    var mess = "";
    if (CodeHelper.isNullOrEmpty(this.props.staffId)) mess = "StaffID is empty";
    if (CodeHelper.isNullOrEmpty(this.props.password))
      mess +=
        (CodeHelper.isNullOrEmpty(mess) ? "" : "\n") + "Password is empty";
    return mess;
  };

  login = async () => {
    if (__DEV__) {
      this.props.setPassword("123456");
      this.props.setStaffId("albertwoo@live.com");
    }
    var mess = this.validLogin();
    if (!CodeHelper.isNullOrEmpty(mess)) {
      Alert.alert(
        "Errors",
        mess,
        [{ text: "OK", onPress: () => console.log("OK Pressed") }],
        { cancelable: false }
      );
      return;
    }
    this.props.loginRequest(this.props.staffId, this.props.password);
  };

  loginComplete = responseJson => {
    console.log(responseJson);
    // Common.ACCESS_TOKEN = responseJson.access_token;
    // Common.STAFF_ID = this.props.staffId;
    this.openMainView();
  };

  render() {
    return (
      <View style={[StyleSheets.container]}>
        <View style={[StyleSheets.container, { justifyContent: "flex-start"}]}>
          <Image
            source={
              require("../assets/images/bg_login.png")
            }
            style={{ resizeMode: "stretch", width: Dimensions.get('window').width, height: 250, marginBottom: 25 }}
          />
          <TextInput
            placeholder={
              LangResources.Languages[Common.CURRENT_LANG].enter_staff_id
            }
            style={[StyleSheets.editText, { height: 40, width: 300 }]}
            underlineColorAndroid="transparent"
            onChangeText={this.onStaffIdChange}
          >
            {this.props.staffId}
          </TextInput>
          <TextInput
            placeholder={
              LangResources.Languages[Common.CURRENT_LANG].enter_password
            }
            style={[StyleSheets.editText, { height: 40, width: 300 }]}
            underlineColorAndroid="transparent"
            onChangeText={this.onPasswordChange}
            secureTextEntry={true}
          >
            {this.props.password}
          </TextInput>

          <TouchableOpacity onPress={this.onLogin}>
            <Image
              source={ImageResources.Images[Common.CURRENT_LANG].ic_ok}
              style={{
                resizeMode: "cover",
                marginTop: 25,
                width: 180,
                height: 35
              }}
            />
          </TouchableOpacity>
        </View>
        {this.props.waitting && (
          <View style={[StyleSheets.loading]}>
            <ActivityIndicator size="large" color="#0000ff" />
          </View>
        )}
      </View>
    );
  }
}

const mapStateToProps = state => ({
  waitting: state.loginReducer.waitting,
  staffId: state.loginReducer.staffId,
  password: state.loginReducer.password,
  response: state.loginReducer.response,
  messageErrors: state.loginReducer.messageErrors
});

const mapDispatchToProps = dispatch => {
  return {
    setStaffId: staffId => {
      dispatch(setStaffId(staffId));
    },
    setPassword: password => {
      dispatch(setPassword(password));
    },
    loginRequest: (staffId, pass) => {
      dispatch(loginRequest(staffId, pass));
    }
  };
};
export default (LoginPageContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(LoginPage));
