import React, { Component } from 'react';
import {
    Platform,
    StyleSheet,
    Text,
    View, Image, TouchableOpacity
} from 'react-native';
import CodeHelper from '../../utils/CodeHelper';
export default class TabItem extends Component {
    render() {
        return (
            <TouchableOpacity onPress={this.props.onPressTabItem} style={{ height: '100%', width: '20%' }}>
                <View style={{ width: '100%', height: '100%', alignItems: 'center', justifyContent: 'center', flexDirection: 'column' }}>
                    <Image source={this.props.tabIcon} style={{ width: '100%', resizeMode: 'center', height: 25, width: 25, }} />
                    <Text style={{ width: '100%', height: 20, textAlign: 'center', color: this.props.textTabColor }}>{this.props.tabText}</Text>
                    { !CodeHelper.isNull(this.props.numberBadge) &&
                        <Text style={[styles.iconBadge]}>{this.props.numberBadge}</Text>
                    }                    
                </View>
            </TouchableOpacity>
        );
    }
}


const styles = StyleSheet.create({
    iconBadge: {
        flex: 1,
        position: 'absolute',
        top: 1,
        right: 10,
        width: 15,
        height: 15,
        fontSize: 10,
        borderRadius: 15,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#FF0000',
        color: '#ffffff',
        fontWeight: 'bold',
        textAlign: 'center',

    },
});