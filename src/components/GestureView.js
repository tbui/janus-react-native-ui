import React, {Component} from 'react';
import { View, Text, PanResponder, ActivityIndicator } from 'react-native';
import PropTypes from 'prop-types';
import {DURATION_SHORT, DURATION_MEDIUM, DURATION_LONG} from '../config/cameraControlTime'

let smallDistance = 20;
let mediumDistance = 60;
let largeDistance = 100;

function calcDistance(x1, y1, x2, y2) {
    let dx = Math.abs(x1 - x2)
    let dy = Math.abs(y1 - y2)
    return Math.sqrt(Math.pow(dx, 2) + Math.pow(dy, 2));
}

class GestureView extends Component {
    static propTypes = {
        onZoomIn: PropTypes.func,
        onZoomOut: PropTypes.func,
        onSwipeLeft: PropTypes.func,
        onSwipeRight: PropTypes.func,
        onSwipeTop: PropTypes.func,
        onSwipeBottom: PropTypes.func,
        contentView: PropTypes.element,
      }
    
      static defaultProps = {
        onZoomIn: () => (console.log('Zoom In!')),
        onZoomOut: () => (console.log('Zoom Out!')),
        onSwipeLeft: () => (console.log('Swipe Left!')),
        onSwipeRight: () => (console.log('Swipe Right!')),
        onSwipeTop: () => (console.log('Swipe Top!')),
        onSwipeBottom: () => (console.log('Swipe Bottom!')),
        contentView: (
          <View style={{
            flex: 1,
            alignItems: 'center',
            justifyContent: 'center'
          }}>
            <Text style={{
              textAlign: 'center',
              fontSize: 16,
              color: "#ffffff"
            }}>
              Place content here
            </Text>
          </View>
        ),        
      }


    constructor(props) {
        super(props);

        this.state = {
            startX: null,
            startY: null,
            currentX: null,            
            currentY: null,
            startDistance: null,
            currentDistance: null,
            isMoving: false,
            isZooming: false
        }
    }

    processPan(x, y) {
        if (!this.state.isMoving) {
            this.setState({
                isMoving: true,
                startX: x,
                startY: y
            });
        } else {
            this.setState({
                currentX: x,
                currentY: y
            });
        }
    }

    processPinch(x1, y1, x2, y2) {
        let distance = calcDistance(x1, y1, x2, y2);
        if (!this.state.isZooming) {
            this.setState({
                isZooming: true,
                startDistance: distance
            });
        } else {
            this.setState({
                currentDistance: distance
            });
        }
    }

    processHandle(handle, distance){
        if(handle){
            handle(distance);    
        }
    }

    componentWillMount() {
        this._panResponder = PanResponder.create({
            onStartShouldSetPanResponder: (evt, gestureState) => true,
            onStartShouldSetPanResponderCapture: (evt, gestureState) => true,
            onMoveShouldSetPanResponder: (evt, gestureState) => true,
            onMoveShouldSetPanResponderCapture: (evt, gestureState) => true,
            onPanResponderGrant: (evt, gestureState) => {},
            onPanResponderMove: (evt, gestureState) => {
                let touches = evt.nativeEvent.touches;
                if (touches.length == 2) {
                    this.processPinch(touches[0].locationX, touches[0].locationY, touches[1].locationX, touches[1].locationY);
                } else if (touches.length == 1 && !this.state.isZooming) {
                    this.processPan(touches[0].locationX, touches[0].locationY);
                }
            },

            onPanResponderTerminationRequest: (evt, gestureState) => true,
            onPanResponderRelease: (evt, gestureState) => {
                if(this.state.isZooming) {
                    if(this.state.currentDistance - this.state.startDistance > smallDistance){
                        this.processHandle(this.props.onZoomIn, this.state.currentDistance - this.state.startDistance);
                    }else if(this.state.startDistance - this.state.currentDistance > smallDistance){
                        this.processHandle(this.props.onZoomOut, this.state.startDistance - this.state.currentDistance);
                    }
                }else if(this.state.isMoving){
                    let horizontal = Math.abs(this.state.startY - this.state.currentY) <= Math.abs(this.state.startX - this.state.currentX);
                    if(this.state.startX - this.state.currentX > smallDistance  && horizontal){
                        this.processHandle(this.props.onSwipeLeft, this.state.startX - this.state.currentX);
                    }else if(this.state.currentX - this.state.startX > smallDistance && horizontal){
                        this.processHandle(this.props.onSwipeRight, this.state.currentX - this.state.startX);
                    }else if(this.state.startY - this.state.currentY > smallDistance){
                        this.processHandle(this.props.onSwipeTop, this.state.startY - this.state.currentY );
                    }else if(this.state.currentY - this.state.startY > smallDistance){
                        this.processHandle(this.props.onSwipeBottom,this.state.currentY - this.state.startY);
                    }
                }

                this.setState({
                    isZooming: false,
                    isMoving: false
                });
            },
            onPanResponderTerminate: (evt, gestureState) => {},
            onShouldBlockNativeResponder: (evt, gestureState) => true,
        });
    }

    render() {
        return (
          <View
            style={[this.props.style]}
            {...this._panResponder.panHandlers}>
             {this.props.contentView}
             {this.props.waitting && (
                <View style={[StyleSheets.loading]}>
                    <ActivityIndicator size="large" color="#eeeeee" />
                </View>
                )}
          </View>
        );
    }

}

export default GestureView;
