import React from "react";
import { View, Alert, StyleSheet } from "react-native";

import {
  RTCPeerConnection,
  RTCMediaStream,
  RTCIceCandidate,
  RTCSessionDescription,
  RTCView,
  RTCVideoView,
  MediaStreamTrack,
  getUserMedia
} from "react-native-webrtc";
import Janus from "../lib/janus.mobile";

let server = "wss://y2ding.syndicatetg.com:6503";
let janus;
let streaming = null;
let started = false;

const VIDEO = 1;
const WAVEFORMS = 2;

Janus.init({
  debug: "all",
  callback: function() {
    if (started) return;
    started = true;
  }
});

export default class VideoControl extends React.Component {
  constructor() {
    super();

    this.state = {
      info: "Initializing",
      status: "init",
      selfViewSrc: null,
      selfViewSrcKey: null,
      streams: {},
      streamHandles: {}
    };
  }
  async componentWillMount() {}
  async componentDidMount() {
    //InCallManager.start({ media: 'video' });
    this.startSession(VIDEO);
  }

  startSession = index => {
    var turn = {
      url: "turn:y2ding.eastus.cloudapp.azure.com:3478",
      username: "webuser",
      credential: "RedSox2004"
    };
    var stun = { url: "stun:stun.l.google.com:19302" };
    this.setState({
      initialized: true,
      visible: true
    });
    janus = new Janus({
      iceServers: [stun, turn],
      server: server,
      success: () => {
        janus.attach({
          plugin: "janus.plugin.streaming",
          success: function(pluginHandle) {
            streaming = pluginHandle;
            Janus.log(
              "Plugin attached! (" +
                streaming.getPlugin() +
                ", id=" +
                streaming.getId() +
                ")"
            );
            // start the stream
            var body = {
              request: "watch",
              id: index
            };
            streaming.send({
              message: body
            });
          },
          error: error => {
            Janus.error("  -- Error attaching plugin... ", error);
            alert("Error attaching plugin... " + error);
          },
          onmessage: (msg, jsep) => {
            Janus.debug(" ::: Got a message :::");
            Janus.debug(msg);
            var result = msg["result"];
            if (result !== null && result !== undefined) {
              if (result["status"] !== undefined && result["status"] !== null) {
                var status = result["status"];
                if (status === "stopped") this.stopSession();
              } else if (msg["streaming"] === "event") {
                // Is simulcast in place?
              }
            } else if (msg["error"] !== undefined && msg["error"] !== null) {
              Alert.alert(msg["error"]);
              this.stopSession();
              return;
            }
            if (jsep !== undefined && jsep !== null) {
              Janus.debug("Handling SDP as well...");
              Janus.debug(jsep);
              // Offer from the plugin, let's answer
              streaming.createAnswer({
                jsep: jsep,
                media: {
                  audioSend: false,
                  videoSend: false
                }, // We want recvonly audio/video
                success: function(jsep) {
                  Janus.debug("Got SDP!");
                  Janus.debug(jsep);
                  var body = {
                    request: "start"
                  };
                  streaming.send({
                    message: body,
                    jsep: jsep
                  });
                  //
                },
                error: function(error) {
                  Janus.error("WebRTC error:", error);
                  Alert.alert("WebRTC error... " + JSON.stringify(error));
                }
              });
            }
          },
          onremotestream: stream => {
            Janus.debug(" ::: Got a remote stream :::");
            Janus.debug(stream);
            const streams = this.state.streams;
            const streamHandles = this.state.streamHandles;
            streams[index] = stream.toURL();
            streamHandles[index] = streaming;
            // this.setState({ selfViewSrc: stream.toURL() });
            // this.setState({ selfViewSrcKey: Math.floor(Math.random() * 1000) });
            this.setState({ streams: streams, streamHandles: streamHandles });
            this.setState({ visible: false });
            if (index === VIDEO) this.setState({ status: "streaming-video" });
          },
          oncleanup: () => {
            Janus.log(" ::: Got a cleanup notification :::");
          }
        });
      }
    });
  };

  stopSession = () => {
    var body = {
      request: "stop"
    };
    streaming.send({
      message: body
    });
    streaming.hangup();
    janus.destroy();
    this.setState({ status: "ready" });
  };

  render() {
    return (
      <View style={[styles.cameraContainer, { backgroundColor: "#ffffff" }]}>
        {this.state.selfViewSrc && (
          <RTCView
            key={this.state.selfViewSrcKey}
            streamURL={this.state.selfViewSrc}
            style={{flex: 1}}
          />
        )}
        {this.state.streams &&
          Object.keys(this.state.streams).map((key, index) => {
            return (
              <RTCView
                key={Math.floor(Math.random() * 1000)}
                streamURL={this.state.streams[key]}
                style={{flex: 1}}
              />
            );
          })}
      </View>
    );
  }
}

const styles = StyleSheet.create({
    cameraContainer: {            
      width: "100%",
      height: 300
    },  
  });
