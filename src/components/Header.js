import React from "react";
import {
  TouchableOpacity,
  StyleSheet,
  View,
  Image,
  Text,
  Platform
} from "react-native";
import CodeHelper from "../utils/CodeHelper";
import { NetInfo } from "react-native";
export default class Header extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isConnected: NetInfo.isConnected
    };
  }

  onSettingsPress = () => {};

  componentDidMount() {
    NetInfo.isConnected.addEventListener(
      "connectionChange",
      this.handleFirstConnectivityChange
    );
  }

  componentWillUnmount() {
    NetInfo.isConnected.removeEventListener(
      "connectionChange",
      this.handleFirstConnectivityChange
    );
  }

  handleFirstConnectivityChange = isConnected => {
    this.setState({ isConnected: isConnected });
  };

  render() {
    return (
      <View
        style={{
          height: Platform.OS == "android" ? 60 : 80,
          backgroundColor: "#353737",
          flexDirection: "row",
          paddingTop: Platform.OS == "android" ? 0 : 20
        }}
      >
        <TouchableOpacity
          onPress={this.props.onSettingsPress}
          style={{ justifyContent: "center" }}
        >
          <Image
            alignSelf="center"
            source={require("../assets/images/ic_menu.png")}
            style={{
              resizeMode: "contain",
              height: 25,
              width: 25,
              justifyContent: "center",
              marginLeft: 10
            }}
          />
        </TouchableOpacity>
        <View style={{ flex: 1 }} />

        {this.props.actions != null &&
          this.props.actions.map((key, index) => {
            return (
              <TouchableOpacity
                key={key.key}
                onPress={key.action}
                style={{ justifyContent: "center" }}
              >
                <Image
                  source={key.icon}
                  style={{
                    resizeMode: "contain",
                    height: 30,
                    width: 30,
                    marginRight: 5
                  }}
                />
              </TouchableOpacity>
            );
          })}

        {!CodeHelper.isNull(this.props.staffId) && (
          <View
            style={{
              height: "100%",
              alignItems: "flex-start",
              justifyContent: "center"
            }}
          >
            <Text style={{ color: "#ffffff", fontSize: 10 }}>
              Staff: {this.props.staffId}
            </Text>
          </View>
        )}

        <Image
          alignSelf="center"
          source={
            this.state.isConnected
              ? require("../assets/images/ic_network_on.png")
              : require("../assets/images/ic_network_off.png")
          }
          style={{
            resizeMode: "contain",
            height: 30,
            width: 30,
            justifyContent: "center",
            marginRight: 5
          }}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "column",
    backgroundColor: "#eaebed",
    alignItems: "center",
    justifyContent: "center"
  }
});
