import React from "react";
import { View, Text } from "react-native";
import StyleSheets from "../utils/StyleSheets";

export default class MeasureView extends React.Component {
  constructor(props) {
    super(props);

    this.state = props.data;
  }

  render() {
    return (
      <View
        style={[
          StyleSheets.dashGrayColor,
          {
            flex: 1,
            alignItems: "center",
            justifyContent: "center",
            borderColor: "#242f45",
            borderWidth: 1
          }
        ]}
      >
        <Text
          style={{
            position: "absolute",
            top: 3,
            left: 3,
            color: "#ef3def",
            fontSize: 10
          }}
        >
          {this.state.title}
        </Text>
        <Text style={{ color: "#ffffff", fontSize: 16, fontWeight: "bold" }}>
          {this.state.value}
        </Text>
        <Text
          style={{
            position: "absolute",
            right: 3,
            bottom: 3,
            color: "#e93e3b",
            fontSize: 8
          }}
        >
          {this.state.unit}
        </Text>
      </View>
    );
  }
}
