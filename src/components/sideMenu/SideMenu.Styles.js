export default {
  container: {
    flex: 1,
    backgroundColor: "#14455a",
    justifyContent: "flex-start"
  },
  userZone: {
    flexDirection: "row",
    height: 120,    
    alignItems: "center"
  },
  userImage: {
    resizeMode: "contain",
    height: 100,
    width: 150,
    justifyContent: "center"
  },
  userInfo: {
    flex: 1,
  },
  infoHeader: {
    color: "#ffffff",
    fontSize: 18,
    fontWeight: "bold"
  },
  infoContent: {
    color: "#ffffff",
    fontSize: 14,
  },
  sectionHeadingStyle: {
    padding: 10,
    fontSize: 16,
    fontWeight: "bold",
    backgroundColor: "#aaaaaa"
  },
  navSectionStyle: {
    flexDirection: "row",
    paddingVertical: 5,
    paddingHorizontal: 10,
    alignItems: "center",
    backgroundColor: "#eeeeee"
  },
  navItemStyle: {
    fontSize: 14,
  },
  
  navItemImage: {
    resizeMode: "contain",
    height: 30,
    width: 30,
    justifyContent: "center"
  },
};
