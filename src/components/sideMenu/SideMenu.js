/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from "react";
import styles from "./SideMenu.Styles";
import { NavigationActions } from "react-navigation";
import { ScrollView, Text, View, Image, TouchableOpacity } from "react-native";
import Common from "../../utils/Common";

class SideMenu extends Component {
  navigateToScreen = route => () => {
    const navigateAction = NavigationActions.navigate({
      routeName: route
    });
    this.props.navigation.dispatch(navigateAction);
  };

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.userZone}>
          <Image
            alignSelf="center"
            style={styles.userImage}
            source={require("../../assets/images/logo_light.png")}
          />
          <View style={styles.userInfo}>
            <Text style={styles.infoHeader}>TeamICU</Text>
            <Text style={styles.infoContent}>Staff: {Common.STAFF_ID}</Text>
          </View>
        </View>
        <ScrollView>
          <View>
            <Text style={styles.sectionHeadingStyle}>Rooms</Text>
            <TouchableOpacity
              style={styles.navSectionStyle}
              onPress={this.navigateToScreen("LiveStream")}
            >
              <Image
                alignSelf="center"
                style={styles.navItemImage}
                source={require("../../assets/images/ic_home.png")}
              />
              <Text style={styles.navItemStyle}>ICU 01</Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={styles.navSectionStyle}
              onPress={this.navigateToScreen("LiveStream")}
            >
              <Image
                alignSelf="center"
                style={styles.navItemImage}
                source={require("../../assets/images/ic_home.png")}
              />
              <Text style={styles.navItemStyle}>ICU 02</Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={styles.navSectionStyle}
              onPress={this.navigateToScreen("LiveStream")}
            >
              <Image
                alignSelf="center"
                style={styles.navItemImage}
                source={require("../../assets/images/ic_home.png")}
              />
              <Text style={styles.navItemStyle}>ICU 03</Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={styles.navSectionStyle}
              onPress={this.navigateToScreen("LiveStream")}
            >
              <Image
                alignSelf="center"
                style={styles.navItemImage}
                source={require("../../assets/images/ic_home.png")}
              />
              <Text style={styles.navItemStyle}>ICU 04</Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={styles.navSectionStyle}
              onPress={this.navigateToScreen("LiveStream")}
            >
              <Image
                alignSelf="center"
                style={styles.navItemImage}
                source={require("../../assets/images/ic_home.png")}
              />
              <Text style={styles.navItemStyle}>ICU 05</Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={styles.navSectionStyle}
              onPress={this.navigateToScreen("LiveStream")}
            >
              <Image
                alignSelf="center"
                style={styles.navItemImage}
                source={require("../../assets/images/ic_home.png")}
              />
              <Text style={styles.navItemStyle}>ICU 06</Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={styles.navSectionStyle}
              onPress={this.navigateToScreen("LiveStream")}
            >
              <Image
                alignSelf="center"
                style={styles.navItemImage}
                source={require("../../assets/images/ic_home.png")}
              />
              <Text style={styles.navItemStyle}>ICU 07</Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={styles.navSectionStyle}
              onPress={this.navigateToScreen("LiveStream")}
            >
              <Image
                alignSelf="center"
                style={styles.navItemImage}
                source={require("../../assets/images/ic_home.png")}
              />
              <Text style={styles.navItemStyle}>ICU 08</Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={styles.navSectionStyle}
              onPress={this.navigateToScreen("LiveStream")}
            >
              <Image
                alignSelf="center"
                style={styles.navItemImage}
                source={require("../../assets/images/ic_home.png")}
              />
              <Text style={styles.navItemStyle}>ICU 09</Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={styles.navSectionStyle}
              onPress={this.navigateToScreen("LiveStream")}
            >
              <Image
                alignSelf="center"
                style={styles.navItemImage}
                source={require("../../assets/images/ic_home.png")}
              />
              <Text style={styles.navItemStyle}>ICU 10</Text>
            </TouchableOpacity>
          </View>
        </ScrollView>
      </View>
    );
  }
}

export default SideMenu;
