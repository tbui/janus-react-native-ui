import HttpStatus from "./HttpStatus";
import Common from "../utils/Common";
import CodeHelper from "../utils/CodeHelper";

export default class HttpClient {
    static getData = async (url, accessToken) => {
        if (CodeHelper.isNull(accessToken)) {
            accessToken = Common.ACCESS_TOKEN;
        }
        try {
            var response = await fetch(url, {
                method: "GET",
                headers: { 'Authorization': 'bearer ' + accessToken }
            }
            )
            result = await response.json();
         
            if (response.status == 200) {
                return { status: HttpStatus.OK, result: result }
            } else {
                return { status: HttpStatus.FAIL, result: "", errors: JSON.stringify(result)  }
            }

        } catch (error) {
            return { status: HttpStatus.FAIL, result: "", errors: error.toString() }
        }
    }

    static postData = async (url, bodyData, accessToken, headerData ) => {
        try {

            if (CodeHelper.isNull(accessToken)) {
                accessToken = Common.ACCESS_TOKEN;
            }
            if (CodeHelper.isNull(headerData)) {
                headerData = {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': 'bearer ' + accessToken
                }
            }
            var response = await fetch(url, {
                method: "POST",
                headers: headerData,
                body: bodyData
            })
            var result = await response.json();
      
            if (response.status == 200) {
                return { status: HttpStatus.OK, result: result }
            } else {
                return { status: HttpStatus.FAIL, result: "", errors: JSON.stringify(result) }
            }

        } catch (error) {
            return { status: HttpStatus.FAIL, result: "", errors: error.toString() }
        }
    }



}