export default class English {
    static Language = {
        enter_staff_id:'Enter Staff ID',
        enter_password:'Enter Password',        
        new_app_version:'Need to update new App Version, Do you want to update now?'
    }
}