import Common from "../utils/Common";
import HttpClient from "../services/HttpClient";
import Config from "../config/config"

export const  loginApi = (action) => new Promise(resolve => {
    var details = {   
        'client_id': '6d1029c8-88c6-4781-8300-fca1f493ff77',     
        'client_secret': '6d1029c8-88c6-4781-8300-fca1f493ff77',
        'username': action.data.staffId,
        'password': action.data.password,
        'grant_type': 'password'
    };
    var formBody = [];
    for (var property in details) {
        var encodedKey = encodeURIComponent(property);
        var encodedValue = encodeURIComponent(details[property]);
        formBody.push(encodedKey + "=" + encodedValue);
    }
    formBody = formBody.join("&");
    console.log("BODY LOGIN", formBody);
    var url = Config.APIURL + '/auth';
    var header = { 'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8' };
    var response = HttpClient.postData(url, formBody, '', header);
    resolve(response);
});
