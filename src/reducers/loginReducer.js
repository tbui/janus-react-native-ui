import { SET_PASSWORD, SET_STAFF_ID, LOGIN_SUCCESS, LOGIN_FAIL } from '../actions/types/loginType';
import { SHOW_WAITTING, HIDE_WAITTING } from '../actions/types/commonType';
import LoginPage from '../page/LoginPage';
const initialState = { waitting: false, staffId: '', password: '', response: undefined, messageErrors: '' }
export default function (state = initialState, action) {
  if (action.name == undefined || action.name != LoginPage.PAGE_NAME) return state;
  switch (action.type) {
    case SHOW_WAITTING:
      return { waitting: true };
    case HIDE_WAITTING:
      return { waitting: false };
    case SET_PASSWORD:
      return { ...state, password: action.data.password };
    case SET_STAFF_ID:
      return { ...state, staffId: action.data.staffId };
    case LOGIN_SUCCESS:
      return { response: action.data };
    case LOGIN_FAIL:
      return { messageErrors: action.error };
    default:
      return state;
  }
}