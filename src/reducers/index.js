import { combineReducers } from 'redux';
import loginReducer from './loginReducer';
import homeReducer from './homeReducer';
import liveStreamReducer from './liveStreamReducer';

export default combineReducers({
  loginReducer,
  homeReducer,
  liveStreamReducer
});