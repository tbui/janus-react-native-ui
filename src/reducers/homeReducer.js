import { REFRESH_PAGE } from "../actions/types/commonType";
import { SHOW_WAITTING, HIDE_WAITTING } from '../actions/types/commonType';
import HomePage from "../page/HomePage";

export default function (state = { refreshing: false, waitting: false, response: undefined, messageErrors: '' }, action) {
  if (action.name == undefined || action.name != HomePage.PAGE_NAME) return state;
  switch (action.type) {
    case REFRESH_PAGE:
      return { refreshing: !state.refreshing };
    case SHOW_WAITTING:
      return { waitting: true };
    case HIDE_WAITTING:
      return { waitting: false };    
    default:
      return state;
  }
}