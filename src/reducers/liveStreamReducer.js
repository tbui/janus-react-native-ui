import { LIVESTREAM_SUCCESS, LIVESTREAM_FAIL } from "../actions/types/liveStreamType";
import { SHOW_WAITTING, HIDE_WAITTING, SET_ORIENTATION } from '../actions/types/commonType';
import LiveStreamPage from "../page/LiveStreamPage";

export default function (state = { waitting: false, messageErrors: '' }, action) {
    if (action.name == undefined || action.name != LiveStreamPage.PAGE_NAME) return state;
    switch (action.type) {
        case SHOW_WAITTING:
            return { waitting: true };
        case HIDE_WAITTING:
            return { waitting: false };     
        case SET_ORIENTATION:
            return { messageErrors: "", orientation: action.orientation };               
        case LIVESTREAM_SUCCESS:
            return { messageErrors: "", responseLiveStream: action.data };
        case LIVESTREAM_FAIL:
            return { messageErrors: action.error };
        default:
            return state;
    }
}