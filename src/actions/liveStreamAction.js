import { SHOW_WAITTING, HIDE_WAITTING, SET_ORIENTATION } from './types/commonType';
import LiveStreamPage from '../page/LiveStreamPage';
import { LIVESTREAM_FAIL, LIVESTREAM_SUCCESS, LIVESTREAM_REQUEST } from './types/liveStreamType';

export const showWaitting = () => ({ type: SHOW_WAITTING, name: LiveStreamPage.PAGE_NAME });
export const hideWaitting = () => ({ type: HIDE_WAITTING, name: LiveStreamPage.PAGE_NAME });

export const setOrientation = (orientation) => ({ 
    type: SET_ORIENTATION, 
    orientation: orientation,
    name: LiveStreamPage.PAGE_NAME });

export const liveStreamRequest = (code) => ({
    type: LIVESTREAM_REQUEST,
    data: {code: code},
    name: LiveStreamPage.PAGE_NAME
});

export const liveStreamSuccess = (response) => ({
    type: LIVESTREAM_SUCCESS,
    data: response,
    name: LiveStreamPage.PAGE_NAME
})

export const liveStreamFail = (error) => ({
    type: LIVESTREAM_FAIL,
    error: error,
    name: LiveStreamPage.PAGE_NAME
})
