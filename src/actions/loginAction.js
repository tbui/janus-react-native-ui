import { LOGIN_SUCCESS, LOGIN_FAIL, LOGIN_REQUEST, SET_PASSWORD, SET_STAFF_ID } from './types/loginType';
import { SHOW_WAITTING, HIDE_WAITTING } from './types/commonType';
import LoginPage from '../page/LoginPage';

export const showWaitting = () => ({ type: SHOW_WAITTING, name: LoginPage.PAGE_NAME });
export const hideWaitting = () => ({ type: HIDE_WAITTING, name: LoginPage.PAGE_NAME });

export const setPassword = (password) => ({ type: SET_PASSWORD, data: { password }, name: LoginPage.PAGE_NAME });
export const setStaffId = (staffId) => ({ type: SET_STAFF_ID, data: { staffId }, name: LoginPage.PAGE_NAME });


export const loginRequest = (staffId, password) => {
    return {
        type: LOGIN_REQUEST,
        data: {
            staffId: staffId,
            password: password,
        }, name: LoginPage.PAGE_NAME
    }
}

export const loginSuccess = (response) => {
    return {
        type: LOGIN_SUCCESS,
        data: response, name: LoginPage.PAGE_NAME
    }
}

export const loginError = (errorMessage) => {
    return {
        type: LOGIN_FAIL,
        error: errorMessage,
        name: LoginPage.PAGE_NAME
    }
}
