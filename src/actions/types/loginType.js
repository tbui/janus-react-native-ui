
export const LOGIN_REQUEST = 'LOGIN_REQUEST'
export const LOGIN_SUCCESS = 'LOGIN_SUCCESS';
export const LOGIN_FAIL = 'LOGIN_FAIL';
export const SET_STAFF_ID = 'SET_STAFF_ID';
export const SET_PASSWORD = 'SET_PASSWORD';