import { REFRESH_PAGE } from "./types/commonType";
import HomePage from "../page/HomePage";
import { SHOW_WAITTING, HIDE_WAITTING } from './types/commonType';

export const refreshHomePage = () => ({ type: REFRESH_PAGE, name: HomePage.PAGE_NAME });
export const showWaitting = () => ({ type: SHOW_WAITTING, name: HomePage.PAGE_NAME });
export const hideWaitting = () => ({ type: HIDE_WAITTING, name: HomePage.PAGE_NAME });