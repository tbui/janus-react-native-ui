let url = null;

if (__DEV__) {
	url = {
		APIURL: "http://vy-pc/TeamICU"
	};
} else {
	url = {
		APIURL: "https://qa.teamicu.com"
	};
}

url.wss = "wss://ws1.teamicu.com:6503";
url.stun = {
	url: "stun:stun.l.google.com:19302"
}
url.turn = {
	url: "turn:y2ding.eastus.cloudapp.azure.com:3478",
	username: "webuser",
	credential: "RedSox2004"
}

export default url;