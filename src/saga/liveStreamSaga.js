import { liveStreamApi } from '../api/liveStreamApi';
import { LIVESTREAM_REQUEST } from "../actions/types/liveStreamType";
import HttpStatus from "../services/HttpStatus";
import { showWaitting, hideWaitting, liveStreamSuccess, liveStreamFail } from "../actions/liveStreamAction";
import {
    takeEvery,
    takeLatest,
    call,
    put
} from 'redux-saga/effects';

//=========================================VALIDATE=========================================
function* liveStreamFlow(code) {
    try {
        yield put(showWaitting());
        const response = yield call(liveStreamApi, code);
        yield put(hideWaitting());
        if (response.status == HttpStatus.OK) {
            yield put(liveStreamSuccess(response.result));
        } else
            yield put(liveStreamFail(response.errors))
    } catch (err) {
        yield put(hideWaitting());
        yield put(liveStreamFail(err.message))
    }
}

export function* watchRequestLiveStream() {
    yield takeLatest(LIVESTREAM_REQUEST, liveStreamFlow);
}