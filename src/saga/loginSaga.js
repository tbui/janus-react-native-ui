import Common from "../utils/Common";
import { loginApi } from "../api/loginApi";
import { LOGIN_REQUEST } from "../actions/types/loginType";
import HttpStatus from "../services/HttpStatus";
import {
  loginSuccess,
  loginError,
  showWaitting,
  hideWaitting
} from "../actions/loginAction";
import { takeEvery, takeLatest, call, put } from "redux-saga/effects";
import CodeHelper from "../utils/CodeHelper";

function* loginFlow(staffId, password) {
  yield put(showWaitting());
  try {
    const response = yield call(loginApi, staffId, password);
    if (response.status == HttpStatus.OK) {
      yield put(loginSuccess(response.result));
    } else {
      if (!CodeHelper.isNull(response.errors.error))
        yield put(loginError(response.errors.error));
      else 
        yield put(loginError(response.errors));
    }
  } catch (err) {
    yield put(loginError(err.message));
  }
  yield put(hideWaitting());
}

const TAG = "LOGIN_SAGA";
export function* watchRequestLogin() {
  yield takeLatest(LOGIN_REQUEST, loginFlow);
}
