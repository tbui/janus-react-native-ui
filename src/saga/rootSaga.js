import { watchRequestLogin } from './loginSaga';
import { all } from 'redux-saga/effects'
import { watchRequestLiveStream } from './liveStreamSaga';


export default function* () {
    yield all([
        watchRequestLogin(),        
        watchRequestLiveStream(),
    ])
}