import English from '../assets/lang/en/Lang';
import VietNam from '../assets/lang/vn/Lang';

export default class LangResources {
    static Languages = {
        en: English.Language,
        vn: VietNam.Language     
    }
}