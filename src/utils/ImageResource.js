
export default class ImageResources {
    static Images = {
        en: {
            ic_ok: require('../assets/images/en/ic_ok.png'),
            ic_cancel: require('../assets/images/en/ic_cancel.png'),
            ic_submit: require('../assets/images/en/ic_submit.png'),
            ic_scan_staff_code: require('../assets/images/en/ic_scan_staff_code.png'),
            ic_logout: require('../assets/images/en/ic_logout.png'),
            ic_upload: require('../assets/images/en/ic_upload.png')
        },
        vn: {
            ic_ok: require('../assets/images/vn/ic_ok.png'),
            ic_cancel: require('../assets/images/vn/ic_cancel.png'),
            ic_submit: require('../assets/images/vn/ic_submit.png'),
            ic_scan_staff_code: require('../assets/images/vn/ic_scan_staff_code.png'),
            ic_logout: require('../assets/images/vn/ic_logout.png'),
            ic_upload: require('../assets/images/vn/ic_upload.png')
        }
    }
}