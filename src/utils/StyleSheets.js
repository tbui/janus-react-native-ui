
import { StyleSheet } from 'react-native';
export default StyleSheets = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: '#000000',
    alignItems: 'center',
    justifyContent: 'center',
    alignSelf: "stretch"
  },  
  loading: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    backgroundColor:'rgba(45,45,45,0.7)',
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%'
  },  
  editText: {
    borderRadius: 5,
    backgroundColor: '#ffffff',
    padding: 5,
    marginTop: 5,
  },

  lightBlueColor: {
    backgroundColor: "#2d354a"
  },
  dashBlueColor: {
    backgroundColor: "#242f45"
  },
  dashGrayColor: {
    backgroundColor: "#242426"
  }
});