import { Alert } from 'react-native';
import Common from '../utils/Common';
import AsyncStorageHelper from '../utils/AsyncStorageHelper';
import Keys from '../utils/Keys';
export default class CodeHelper {
    static isNull = (o) => {
        return o == undefined || o == null || o == NaN || 0 == 'null';
    }

    static getBool = (o) => {
        if (CodeHelper.isNull(o)) return false;
        return o;
    }

    static isNullOrEmpty = (o) => {
        if (CodeHelper.isNull(o) || o == '') return true;
        return false;
    }

    static showMessage = (title, mess) => {
        Alert.alert(title, mess, [
            { text: 'OK', onPress: () => console.log('OK Pressed') },
        ], { cancelable: false });
    }

    static getCurrentLang = async () => {
        Common.CURRENT_LANG = await AsyncStorageHelper.getItem(Keys.CURRENT_LANG, '');
        if (CodeHelper.isNullOrEmpty(Common.CURRENT_LANG)) {
            Common.CURRENT_LANG = "en";
            await AsyncStorageHelper.setItem(Keys.CURRENT_LANG, Common.CURRENT_LANG);
        }
    }

    static setCurrentLang = async (lang) => {
        Common.CURRENT_LANG = lang;
        await AsyncStorageHelper.setItem(Keys.CURRENT_LANG, lang);
    }
}