import { AsyncStorage } from 'react-native'
export default class AsyncStorageHelper {
    static setItem = async (key, value) => {
        try {
            await AsyncStorage.setItem(key, value);
            return '';
        } catch (e) {
            return e;
        }
    }

    static getItem = async(key, defaultValue)=>{
        try {
            return await AsyncStorage.getItem(key);
        } catch (e) {
            return defaultValue;
        }
    }

    static removeItem = async(key)=>{
        try {
             await AsyncStorage.removeItem(key);
             return '';
        } catch (e) {
            return e;
        }
    }
}