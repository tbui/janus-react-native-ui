export default class Common {    
    static ACCESS_TOKEN = '';
    static ACCESS_TOKEN_PUBLIC = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJWYWxpZCBVc2VyIFdpdGggU2VyaW91cyBDbGFpbXMiLCJqdGkiOiI3OGM0ODZmYy0zMDU0LTRjMGItYjdlNy1jMmVhNjgzNzYxNTAiLCJodHRwOi8vc2NoZW1hcy5taWNyb3NvZnQuY29tL3dzLzIwMDgvMDYvaWRlbnRpdHkvY2xhaW1zL3JvbGUiOiJBZG1pbiIsImh0dHA6Ly9zY2hlbWFzLnhtbHNvYXAub3JnL3dzLzIwMDUvMDUvaWRlbnRpdHkvY2xhaW1zL25hbWUiOiJWSVBTaWduVVAiLCJleHAiOjE4MTI0OTc1NjksImlzcyI6Imh0dHA6Ly8xMC4xODguMjkuMTIiLCJhdWQiOiJodHRwOi8vMTAuMTg4LjI5LjEyIn0.JK2feN5tMZgC7JlaG3dB5dTskj5TpNTm0z5SaPTz7r8";
    static STAFF_ID = '';
    static APP_ID = 'TeamICU';
    static CURRENT_LANG = '';
    static TOTAL_VOTE = 0;    
}