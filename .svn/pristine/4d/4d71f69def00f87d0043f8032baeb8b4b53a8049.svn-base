import React from "react";
import {
  StyleSheet,
  View,
  Text,
  Alert,
  ActivityIndicator,
  TouchableOpacity,
  Image
} from "react-native";
import Common from "../utils/Common";
import CodeHelper from "../utils/CodeHelper";
import ImageResources from "../utils/ImageResource";
import Header from "../components/Header";
import QRCodeScanner from "react-native-qrcode-scanner";
import { connect } from "react-redux";

class ValidatePage extends React.Component {
  static PAGE_NAME = "ValidatePage";
  constructor() {
    super();

    this.state = {
      zone: "",
      member: "",
      mobile: "",
      invalid: false,
      inUse: false
    };
  }
  async componentWillMount() {}
  async componentDidMount() {}

  componentWillReceiveProps = newProps => {
    if (!CodeHelper.isNullOrEmpty(newProps.messageErrors)) {
      CodeHelper.showMessage("Errors", newProps.messageErrors);
      return;
    }
    if (!CodeHelper.isNull(newProps.responseValidate)) {
      this.validateComplete(newProps.responseValidate);
    }
  };

  validateComplete = responseData => {
    if (responseData.ret_code == "F") {
      CodeHelper.showMessage("Errors", responseData.ret_msg);
    } else {
      this.setState({
        zone: responseData.Zone,
        member: "Member Name: " + responseData.Member,
        mobile: "Member Mobile: " + responseData.Mobile,
        invalid: responseData.Invalid,
        inUse: responseData.InUse
      });
    }
  };

  handleBarCodeRead = data => {
    //this.props.validateRequest(data.data);
  };

  render() {
    return (
      <View style={{ flex: 1, backgroundColor: "#ffffff" }}>
        <Header
          onSettingsPress={() => {
            this.props.navigation.navigate("Settings");
          }}
          alignLogo="baseline"
          paddingLeftLogo={5}
          staffId={Common.STAFF_ID}
        />
        <View style={styles.cameraContainer}>
          <QRCodeScanner
            containerStyle={{ height: "100%", width: "100%" }}
            onRead={this.handleBarCodeRead.bind(this)}
            showMarker={true}
            customMarker={
              <View style={styles.rectangleContainer}>
                <View style={styles.rectangle} />
              </View>
            }
            bottomContent={
              <Text style={styles.bottomText}>
                Place a QR code inside the viewfinder to scan it
              </Text>
            }
            checkAndroid6Permissions={true}
            reactivate={true}
            reactivateTimeout={5000}
            permissionDialogMessage="Ticket validation need camera permission"
          />
        </View>
        <View style={styles.contentHeader}>
          <Text style={styles.contentHeaderTile}>Ticket Status</Text>
        </View>
        {!this.state.invalid &&
          !this.state.inUse && (
            <View style={styles.contentBody}>
              <View style={styles.contentItem}>
                <Text style={{ fontSize: 18 }}>{this.state.zone}</Text>
              </View>
              <View style={styles.contentItem}>
                <Text style={{ fontSize: 18 }}>{this.state.member}</Text>
              </View>
              <View style={styles.contentItem}>
                <Text style={{ fontSize: 18 }}>{this.state.mobile}</Text>
              </View>
            </View>
          )}
        {this.state.invalid && (
          <View style={styles.contentInvalid}>
            <Text style={styles.invalidText}>Invalid Ticket</Text>
          </View>
        )}
        {this.state.inUse && (
          <View style={styles.contentInvalid}>
            <Text style={styles.invalidText}>
              Ticket No. incorrect / Ticket be Used
            </Text>
          </View>
        )}
        <TouchableOpacity style={styles.btnCancel}>
          <Image
            source={ImageResources.Images[Common.CURRENT_LANG].ic_cancel}
            style={{ resizeMode: "center" }}
            resizeMethod="resize"
          />
        </TouchableOpacity>
        {this.props.waitting && (
          <View style={[StyleSheets.loading]}>
            <ActivityIndicator size="large" color="#0000ff" />
          </View>
        )}
      </View>
    );
  }
}

const mapStateToProps = state => ({
  waitting: state.validateReducer.waitting,
  responseValidate: state.validateReducer.responseValidate,
  messageErrors: state.validateReducer.messageErrors
});

const mapDispatchToProps = dispatch => {
  return {
    validateRequest: code => {
      //dispatch(validateRequest(code));
    }
  };
};
export default (ValidatePageContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(ValidatePage));

const styles = StyleSheet.create({
  cameraContainer: {
    height: 250,
    width: "100%"
  },

  rectangleContainer: {
    flex: 1,
    height: "100%",
    width: "100%",
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "transparent"
  },

  rectangle: {
    height: "70%",
    width: "65%",
    borderWidth: 2,
    borderColor: "#00FF00",
    backgroundColor: "transparent"
  },

  bottomText: {
    color: "#fdfdfd",
    fontSize: 12
  },

  contentHeader: {
    alignItems: "center",
    justifyContent: "center",
    height: 40,
    width: "100%",
    backgroundColor: "#4472c4"
  },

  contentHeaderTile: {
    color: "#ffffff",
    fontSize: 16
  },

  contentBody: {
    flexDirection: "column",
    flex: 1,
    alignItems: "flex-start",
    justifyContent: "flex-start",
    height: "100%",
    width: "100%"
  },

  contentInvalid: {
    flexDirection: "column",
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    height: "100%",
    width: "100%"
  },  

  contentItem: {
    flexDirection: "column",
    alignItems: "flex-start",
    justifyContent: "center",
    height: 50,
    width: "100%",
    paddingLeft: 20
  },

  invalidText: {
    color: "#ff0000",
    fontSize: 18
  },

  btnCancel: {
    alignItems: "center",
    justifyContent: "center",
    height: 60,
    width: "100%"
  }
});
